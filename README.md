# README #

# Required #

-Swift 2.0.0

- Use the 'Main'-branch




# Projektrapport #

#Saker vi valde bort

-‘Finare’ sprites och animationer:
Vi valde bort finare sprites med animationer för att prioritera tiden på funktioner och game-mechanics.

-Startskärm:
Vi valde bort en startskärm helt enkelt för att det är en prototyp och vi ville lägga fokus på game-mechanics och funktionen med poänghantering.

-Resultatvyn säger bara Highscore och current score:
 Vi ansåg att för prototypens syfte var det viktigaste att veta poängen spelaren uppnår och ifall en spelare uppnådde nya highscore, annars visas spelarens senaste highscore. Därför valde vi bort annan info.

-Bakgrundsbild:
För prototypens syfte ansåg vi att en enkel färg som bakgrundsvy är tillräcklig istället för att spendera tid på en bakgrundsbild.

-Fysik för Path/Smudges:
Idén var att den ska påverkas av gravitation, mest pga begränsningar i SpriteKit Physics. Alternativa lösningar testades men det skapade bara mer problem pga hur SpriteKit Physics fungerar. Vi valde därför att använda stationära smudges som spelets blobies kan kollidera med.

-Zoom-funktion:
Vi bestämde oss för att enbart panorera vyn horisontellt och ansåg att vid utzoomning finns risken att spelets blobies skulle kunna bli för små.

#Saker som visade sig vara mindre viktiga

-Input delay visade sig inte vara nödvändig, anledningen till varför det togs upp var för att man inte skulle råka trycka sönder blobs/förstöra smudges när man panade men det var helt enkelt ett non-issue.

-Blobs har inget “scared” state utan de fortsätter att gå även fast du smetar ut, tempot i spelet var tillräckligt långsamt ändå.